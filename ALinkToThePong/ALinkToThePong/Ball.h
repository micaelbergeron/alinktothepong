#pragma once
#include "actor.h"
#include "WorldObject.h"
#include <math.h>
#include "Link.h"

// Cette classe impl�mente les fonctionnalit� de la balle de jeu.

#define PI 3.14159265

class Ball :
	public Actor
{
	int m_direction; // Direction exprim� en degr�s (PI/180)
	int m_speed;	
	int m_invulCount;
	Link* m_lastHitter;
public:
	Ball(Bitmap* _idle, POINT _offSet);
	~Ball(void);

	virtual int RefCode(){return 3;};
	virtual SPRITEACTION Update();
	virtual void Reset();

	void Move(int speed);
	void Draw(HDC hDC);

	void SetSpeed(int _speed) {m_speed = _speed;};
	int GetSpeed(){return -m_speed;};

	void SetAngle(int _dir){m_direction = _dir;};
	int GetAngle(){return m_direction;};

	void SetLastHitter(Link* _hitter){m_lastHitter = _hitter;};
	Link* GetLastHitter(){return m_lastHitter;};

	void Bounce(WorldObject* _collided);

	bool IsHittable() {return m_invulCount <= 0;};
	void SetInvulCount(int _count){m_invulCount = _count;};
};
