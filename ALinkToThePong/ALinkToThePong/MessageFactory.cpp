#include "MessageFactory.h"

HDC MessageFactory::msgDC = NULL;

void MessageFactory::SetHDC(HDC hDC)
{
	msgDC = hDC;
}

Message* MessageFactory::createMessage(int msgID, HINSTANCE _hInst)
{
	bool isValid = false;
	switch(msgID)
	{
	case IDB_MSG_OK:
		return new Message(new Bitmap( MessageFactory::msgDC , IDB_MSG_OK, _hInst));
		break;
	case IDB_MSG_PERFECT:
		return new Message(new Bitmap(MessageFactory::msgDC , msgID, _hInst));
		break;
	}

	return NULL;
}