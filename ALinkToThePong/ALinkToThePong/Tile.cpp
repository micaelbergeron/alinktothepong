#include "Tile.h"

Tile::Tile(Bitmap *_bmp) : Sprite(_bmp)
{
	SetZOrder(0);
}

Tile::~Tile(void)
{
}

void Tile::Draw(HDC hDC)
{
	Sprite::Draw(hDC, 0);
}

Tile* Tile::CreateTile(Bitmap *_bmp, bool _doBlock)
{
	Tile* _tmp = new Tile(_bmp);
	_tmp->m_block = _doBlock;

	return _tmp;
}