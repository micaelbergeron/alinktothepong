#include "Menu.h"

Menu::Menu(void)
{
	Show();
}

Menu::~Menu(void)
{
	int max = m_items.size();
	for (int i = 0; i < max; i ++)
		delete m_items[i];
	m_items.clear();

}

void Menu::ManagePress(int x, int y)
{
	for each(Button* btn in m_items)
	{
		if (btn->CheckClick(x, y))
		{
			btn->SetPressed(true);
			break;
		}
	}
}

void Menu::ManageClick(int x, int y)
{
	for each(Button* btn in m_items)
	{		
		if (btn->CheckClick(x, y))
		{		
			btn->Click();
			btn->SetPressed(false);
		}
	}
}

void Menu::AddButton(Button* newButton)
{
	m_items.push_back(newButton);
	newButton->SetParent(this);
}

void Menu::Draw(HDC hDC)
{
	if (m_hidden)
		return;

	for each(Button* btn in m_items)
	{
		btn->Draw(hDC);
	}
}