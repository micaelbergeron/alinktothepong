#pragma once
#include "windows.h"

class ICollision
{
protected:
	bool m_isStatic;
	  RECT  m_rcPosition,
            m_rcCollision;

	virtual void  CalcCollisionRect() = 0;
public:
	virtual ~ICollision(void);
	virtual BOOL TestCollision(ICollision* _text) = 0;

	virtual bool IsStatic(){return m_isStatic;};

	RECT&   GetPosition()             { return m_rcPosition; };
	RECT&   GetCollision()            { return m_rcCollision; };
};
