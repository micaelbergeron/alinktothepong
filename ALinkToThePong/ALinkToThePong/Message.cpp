#include "Message.h"

Message::Message(Bitmap* _bmp) : Sprite(_bmp)
{
	SetNumFrames(5);
	SetFrameDelay(1);
	SetVelocity(0, 0);
}

Message::~Message(void)
{
	Sprite::~Sprite();
}

void Message::OnAnimEnd()
{
	// auto destruction
	delete this;
}

void Message::Draw(HDC hDC)
{
	Sprite::Draw(hDC, 0);
}
