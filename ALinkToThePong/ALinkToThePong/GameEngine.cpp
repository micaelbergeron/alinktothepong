#define _SECURE_SCL 0
//#define _HAS_ITERATOR_DEBUGGING 0

//-----------------------------------------------------------------
// Code source Moteur de jeu/Proc�dure de fen�tre
// C++ Source - GameEngine.cpp
//-----------------------------------------------------------------
// Include
//-----------------------------------------------------------------
#include "GameEngine.h"

//-----------------------------------------------------------------
// Initialisation pointeur statique du moteur du jeu
//-----------------------------------------------------------------
GameEngine *GameEngine::m_pGameEngine = NULL;

//-----------------------------------------------------------------
// Fonction fen�tre principale
//-----------------------------------------------------------------
int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance,
  			  PSTR szCmdLine, int iCmdShow)
{
  MSG         msg;
  static int  iTickTrigger = 0;
  int         iTickCount;

  if (GameInitialize(hInstance))   //fonctions d�initialisation du jeu (�v�nement du jeu)
  {
    // Initialiser le moteur de jeu
    if (!GameEngine::GetEngine()->Initialize(iCmdShow))
      return FALSE;

    // Boucle de messages
    while (TRUE)
    {
      if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
      {
        // traiter les messages
        if (msg.message == WM_QUIT)
          break;
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
      else
      {
        // V�rifier si le jeu est en veille
        if (!GameEngine::GetEngine()->GetSleep())
        {
		// V�rifier si un nouveau cycle doit commencer
          iTickCount = GetTickCount();
          if (iTickCount > iTickTrigger)
          {
            iTickTrigger = iTickCount +
              GameEngine::GetEngine()->GetFrameDelay();
			HandleKey();
            GameCycle();
          }
        }
      }
    }
    return (int)msg.wParam;
  }
  // FIN du jeu
  GameEnd();
  return TRUE;
}


LRESULT CALLBACK WndProc(HWND hWindow, UINT msg, WPARAM wParam, LPARAM lParam)
{
  // Achemine les messages vers le moteur de jeu
  return GameEngine::GetEngine()->HandleEvent(hWindow, msg, wParam, lParam);
}

//-----------------------------------------------------------------
// Game Engine Helper Methods
//-----------------------------------------------------------------
BOOL GameEngine::CheckSpriteCollision(Sprite* pTestSprite)
{
	if (m_world != 0)
		if (m_world->TestCollision(pTestSprite))
		{
			return SpriteCollision(pTestSprite, m_world);
		}

  // See if the sprite has collided with any other sprites
  vector<Sprite*>::iterator siSprite;
  for (siSprite = m_vSprites.begin(); siSprite != m_vSprites.end(); siSprite++)
  {
    // Make sure not to check for collision with itself
    if (pTestSprite == (*siSprite))
      continue;

    // Test the collision
    if (pTestSprite->TestCollision(*siSprite))
      // Collision detected
      return SpriteCollision((*siSprite), pTestSprite);
  }

  // No collision
  return FALSE;
}

//-----------------------------------------------------------------
// GameEngine Constructeur/Destructeur
//-----------------------------------------------------------------
GameEngine::GameEngine(HINSTANCE hInstance, LPTSTR szWindowClass,
  LPTSTR szTitle, WORD wIcon, WORD wSmallIcon, int iWidth, int iHeight)
{
	// Initialise les variables du moteur 
	m_pGameEngine 	= this;			
	m_hInstance	= hInstance;
	m_hWindow 	= NULL;
	if (lstrlen(szWindowClass) > 0)
		lstrcpy(m_szWindowClass, szWindowClass);
	if (lstrlen(szTitle) > 0)
		lstrcpy(m_szTitle, szTitle);
	m_wIcon 	= wIcon;
	m_wSmallIcon = wSmallIcon;
	m_iWidth 	= iWidth;
	m_iHeight 	= iHeight;
	m_iFrameDelay 	= 30;   // par d�faut 20 FPS  (1000/50)
	m_bSleep 	= TRUE;
	m_vSprites.reserve(30);
	m_world = 0;
}

GameEngine::~GameEngine()
{
}

//-----------------------------------------------------------------
// Game Engine : M�thodes g�n�rales
//-----------------------------------------------------------------
// Initialize :Dissimule le code Window g�n�rique dans le moteur du jeu
//
BOOL GameEngine::Initialize(int iCmdShow)
{
  WNDCLASSEX    wndclass;

  // Configure et cr�e fen�tre principale

  wndclass.cbSize		  = sizeof(wndclass);
  wndclass.style          = NULL;
  wndclass.lpfnWndProc    = WndProc;
  wndclass.cbClsExtra     = 0;
  wndclass.cbWndExtra     = 0;
  wndclass.hInstance      = m_hInstance;
  wndclass.hIcon          = LoadIcon(m_hInstance, MAKEINTRESOURCE(GetIcon()));
  wndclass.hIconSm		  = LoadIcon(m_hInstance, MAKEINTRESOURCE(GetSmallIcon()));
  wndclass.hCursor        = LoadCursor(NULL, IDC_ARROW);
  wndclass.hbrBackground  = (HBRUSH)(COLOR_WINDOW + 1);
  wndclass.lpszMenuName   = NULL;
  wndclass.lpszClassName  = m_szWindowClass;

  // Cr�e instance de l�objet fen�tre
  if (!RegisterClassEx(&wndclass))
    return FALSE;

  // Calcule dimension de la fen�tre et la position en fonction de la fen�tre de jeu
 int iWindowWidth = m_iWidth + GetSystemMetrics(SM_CXFIXEDFRAME) * 2;
 int iWindowHeight = m_iHeight + GetSystemMetrics(SM_CYFIXEDFRAME) * 2 +
        GetSystemMetrics(SM_CYCAPTION);
 if (wndclass.lpszMenuName != NULL)
    iWindowHeight += GetSystemMetrics(SM_CYMENU);
  int iXWindowPos = (GetSystemMetrics(SM_CXSCREEN) - iWindowWidth) / 2;
  int  iYWindowPos = (GetSystemMetrics(SM_CYSCREEN) - iWindowHeight) / 2;

  // Cr�er la fen�tre
  m_hWindow = CreateWindow(m_szWindowClass, m_szTitle, 
	  WS_POPUPWINDOW |WS_CAPTION | WS_MINIMIZEBOX | WS_VISIBLE, 
   iXWindowPos, iYWindowPos, iWindowWidth,iWindowHeight,
   NULL, NULL, m_hInstance, NULL);
  if (!m_hWindow)
      return FALSE;
  // Affichage et mise � jour de la fen�tre
  ShowWindow(m_hWindow, iCmdShow);
  UpdateWindow(m_hWindow);

  return TRUE;
}
//-----------------------------------------------------------------
// HandleEvent : Recoit les messages habituellement g�rer dans la fonction WndProc
//-----------------------------------------------------------------
LRESULT GameEngine::HandleEvent(HWND hWindow, UINT msg, WPARAM wParam, LPARAM lParam)
{  // Acheminer les messages Windows vers les FONCTIONS membres du moteur de jeu
  switch (msg)
  {
    case WM_CREATE:			// Actualiser fen�tre de jeu et d�marrer le jeu
      SetWindow(hWindow);
      GameStart(hWindow);       
      return 0;

    case WM_ACTIVATE:		  	// Activer/desactiver le jeu  
      if (wParam != WA_INACTIVE)
      {
        GameActivate(hWindow);	//jeu r�veill�
        SetSleep(FALSE);
      }
      else
      {
        GameDeactivate(hWindow);	//jeu en veille
        SetSleep(TRUE);
      }
      return 0;

    case WM_PAINT:
      HDC         hDC;
      PAINTSTRUCT ps;
      hDC = BeginPaint(hWindow, &ps);

      GamePaint(hDC);		      // Trac� du jeu

      EndPaint(hWindow, &ps);
      return 0;

	case WM_LBUTTONDOWN:
      // Handle left mouse button press
      MouseButtonDown(LOWORD(lParam), HIWORD(lParam), TRUE);
      return 0;

    case WM_LBUTTONUP:
      // Handle left mouse button release
      MouseButtonUp(LOWORD(lParam), HIWORD(lParam), TRUE);
      return 0;

    case WM_RBUTTONDOWN:
      // Handle right mouse button press
      MouseButtonDown(LOWORD(lParam), HIWORD(lParam), FALSE);
      return 0;

    case WM_RBUTTONUP:
      // Handle right mouse button release
      MouseButtonUp(LOWORD(lParam), HIWORD(lParam), FALSE);
      return 0;

    case WM_MOUSEMOVE:
      // Handle mouse movement
      MouseMove(LOWORD(lParam), HIWORD(lParam));
      return 0;

    case WM_DESTROY:
      // Fin du jeu et fermeture de l�application
      GameEnd();
      PostQuitMessage(0);
      return 0;
  }
  return DefWindowProc(hWindow, msg, wParam, lParam);
}

void GameEngine::ErrorQuit(LPTSTR szErrorMsg)
{
  MessageBox(GetWindow(), szErrorMsg, TEXT("Critical Error"), MB_OK | MB_ICONERROR);
  PostQuitMessage(0);
}

void GameEngine::AddSprite(Sprite* pSprite)
{
  // Add a sprite to the sprite vector
  if (pSprite != NULL)
  {
    // See if there are sprites already in the sprite vector
    if (m_vSprites.size() > 0)
    {
      // Find a spot in the sprite vector to insert the sprite by its z-order
      vector<Sprite*>::iterator siSprite;
      for (siSprite = m_vSprites.begin(); siSprite != m_vSprites.end(); siSprite++)
        if (pSprite->GetZOrder() < (*siSprite)->GetZOrder())
        {
          // Insert the sprite into the sprite vector
          m_vSprites.insert(siSprite, pSprite);
          return;
        }
    }
    // The sprite's z-order is highest, so add it to the end of the vector
    m_vSprites.push_back(pSprite);
  }
}

void GameEngine::DrawSprites(HDC hDC)
{
  // Draw the sprites in the sprite vector
  vector<Sprite*>::iterator siSprite;
  for (siSprite = m_vSprites.begin(); siSprite != m_vSprites.end(); siSprite++)
      (*siSprite)->Draw(hDC);
}

void GameEngine::UpdateSprites()
{
  // Update the sprites in the sprite vector
  RECT          rcOldSpritePos;
  SPRITEACTION  saSpriteAction;
  vector<Sprite*>::iterator siSprite;
  for (siSprite = m_vSprites.begin(); siSprite != m_vSprites.end(); siSprite++)
  {
    // Save the old sprite position in case we need to restore it
    rcOldSpritePos = (*siSprite)->GetPosition();

    // Update the sprite
    saSpriteAction = (*siSprite)->Update();

    // Handle the SA_KILL sprite action
    if (saSpriteAction & SA_KILL)
    {
      delete (*siSprite);
      m_vSprites.erase(siSprite);
      siSprite--;
      continue;
    }

	if(!(*siSprite)->IsStatic())
	{
		// See if the sprite collided with any others
		if (CheckSpriteCollision(*siSprite))
		  // Restore the old sprite position
		  (*siSprite)->SetPosition(rcOldSpritePos);
	}
  }
}

void GameEngine::CleanupSprites()
{
  // Delete and remove the sprites in the sprite vector
   for ( int i = 0; i < m_vSprites.size(); i++ ) 
	   delete m_vSprites[i];
   m_vSprites.clear();
}

Sprite* GameEngine::IsPointInSprite(int x, int y)
{
  // See if the point is in a sprite in the sprite vector
  vector<Sprite*>::reverse_iterator siSprite;
  for (siSprite = m_vSprites.rbegin(); siSprite != m_vSprites.rend(); siSprite++)
	   if (!(*siSprite)->IsHidden() && (*siSprite)->IsPointInside(x, y))
		   return (*siSprite);

  // The point is not in a sprite
  return NULL;
}
