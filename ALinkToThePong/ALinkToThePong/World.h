#pragma once

#include "WorldObject.h"
#include <vector>
#include <string>
#include "Sprite.h"
#include "xpath_processor.h"
#include "xpath_static.h"

using namespace std;
/*
Cette classe encapsule le "niveau".
Elle est compos�e d'un vecteur de "Tiles".
*/

class World : public Sprite
{
	std::vector<WorldObject* > m_objects;
	POINT m_startPos[3];

	WorldObject* m_collided;
	World(Bitmap* _bmp);
public:
	static World* CreateFromFile(HDC hDC, char* path);
	~World(void);

	void AddWorldObject(WorldObject* toAdd){m_objects.push_back(toAdd);};	
	void AddStartPos(POINT _startPos, int index);
	
	POINT GetStartPosition(int index){return m_startPos[index];};

	void Draw(HDC hDC);
	virtual BOOL TestCollision(ICollision * pTest);

	WorldObject* GetCollided(){return m_collided;};
};

