//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ALinkToThePong.rc
// Icons 0-100
#define IDB_ICON							1
#define IDB_BIG_ICON					2

// Bitmaps 100-1000
#define IDB_TEST							100
#define IDB_WALK							101
#define IDB_BITMAP1                    102
#define IDB_BG								103
#define IDB_LOGO							104
#define IDB_BTNINST                    105
#define IDB_BTNQUIT                    106
#define IDB_BTNJOUER                 107
#define IDB_BG_INST                    108
#define IDB_BTNJOUER_P				109
#define IDB_BTNINST_P				110
#define IDB_BTNQUIT_P				111
#define IDB_BTNOK						112
#define IDB_BTNOK_P					113
#define IDB_BTNBACK					114
#define IDB_BTNBACK_P				115
#define IDB_ATCK							116
#define IDB_IDLE							117
#define IDB_BALL_BOUNCE			118
#define IDB_BALL_DESTROY			119
#define IDB_MSG_OK						120
#define IDB_MSG_PERFECT			121
#define IDB_HUD							122
#define IDB_BTN_DESERT				123
#define IDB_BTN_FOREST				124
#define IDB_WIN_SPLASH				125

// MP3 Paths
#define MP3_OVERWORLD			"Resource\\audio\\overworld.mp3"
#define MP3_SWING					"Resource\\audio\\sfx_swing.mp3"
#define MP3_SHOP						"Resource\\audio\\shop.mp3"
#define MP3_TRIFORCE				"Resource\\audio\\triforcefanfare.mp3"
#define MP3_BOSS						"Resource\\audio\\bossbgm.mp3"
#define MP3_TITLE						"Resource\\audio\\title.mp3"
#define MP3_TITLE2					"Resource\\audio\\title2.mp3"

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        109
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
