#include "Link.h"

Link::Link(int _dmg, int _maxHp, Bitmap* _idle, POINT _offSet) : Actor(_maxHp, _idle, _offSet)
{
	m_dmg = _dmg;
	m_swingArc = 0;
	m_nbGoals = 0;

	Direction dir;
	dir.HorizontalDir = HOR_NONE;
	dir.VerticalDir = DOWN;
	m_dir = dir;

	sfx = BASS_StreamCreateFile(false, MP3_SWING, 0, 0, 0);
	BASS_ChannelSetAttribute(sfx, BASS_ATTRIB_VOL, 0.20);
}

Link::~Link(void)
{
}

void Link::Attack()
{
	if ( CanDoAction(ATTACKING) && m_exhaust == 0 )
	{
		BASS_ChannelPlay(sfx, false);
		SetState(ATTACKING);
		m_ptVelocity.x = 0;
		m_ptVelocity.y = 0;
	}
}

void Link::DoDamage(Actor* _target)
{
	_target->LoseHP(m_dmg);
}

void Link::OnAnimTick()
{
	if(GetState() == ATTACKING)
	{
		m_swingArc += (90 / Sprite::GetNumFrames()); 
		if (m_swingArc > 90)
			m_swingArc = 0;
	}
	Actor::OnAnimTick();
}

void Link::OnAnimEnd()
{
	if (GetState() == ATTACKING)
	{
		m_exhaust = 20;
	}
	Actor::OnAnimEnd();
}
SPRITEACTION Link::Update()
{
	m_exhaust -= 1;
	m_exhaust = m_exhaust >= 0 ? m_exhaust : 0;
	
	if (m_exhaust == 19)
	{
		SetState(IDLE);
		m_swingArc = 0;
	}
	return Actor::Update();
}

int Link::ContactAngle()
{
	int _angle;
	switch ( m_dir.ToIndex() )
	{
	case 0: // UP
		_angle = 45 + GetSwing();
		break;
	case 1: //RIGHT
		_angle = 45 - GetSwing();
		if (_angle < 0)
			_angle = 360 - _angle;
		break;
	case 2: // DOWN
		_angle = GetSwing() + 225;
		break;
	case 3: // LEFT
		_angle = GetSwing() + 135;
		break;
	}

	return _angle;
}

void Link::Reset()
{
	m_nbGoals = 0;
	m_swingArc = 0;
}


