//-----------------------------------------------------------------
// Game Engine 
// C++ - GameEngine.h
//-----------------------------------------------------------------
#pragma once

//-----------------------------------------------------------------
// Include Files
#include <windows.h>
#include <gdiplus.h>
#include <vector>
#include <mmsystem.h>
#include "Sprite.h"
#include "World.h"
using namespace std;

//-----------------------------------------------------------------
// PROTOTYPES DE LA FONCTION FEN�TRE
//-----------------------------------------------------------------
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                    PSTR szCmdLine, int iCmdShow);
LRESULT CALLBACK  WndProc(HWND hWnd, UINT msg, 
    WPARAM wParam, LPARAM lParam);

//-----------------------------------------------------------------
// Game Engine : Prototypes des autres fonctions
//-----------------------------------------------------------------
BOOL GameInitialize(HINSTANCE hInstance);
void GameStart(HWND hWindow);
void GameEnd();
void GameActivate(HWND hWindow);
void GameDeactivate(HWND hWindow);
void GamePaint(HDC hDC);
void GameCycle();
void HandleKey();
void MouseButtonDown(int x, int y, BOOL bLeft);
void MouseButtonUp(int x, int y, BOOL bLeft);
void MouseMove(int x, int y);
BOOL SpriteCollision(ICollision* pHitter, ICollision* pHittee);

class GameEngine
{
protected: 

	static GameEngine* m_pGameEngine;       // pointeur sur moteur pour permettre l�acc�s de l�ext�rieur 
									 // par une fonction du jeu	
	HINSTANCE	m_hInstance;	 //Instance de l�application et de la fen�tre du jeu
	HWND	m_hWindow;
	TCHAR	m_szWindowClass[32]; //Nom de la classe et titre de la fen�tre du jeu
	TCHAR	m_szTitle[32];
	WORD	m_wIcon, m_wSmallIcon; 	//ID num�rique des ic�nes du jeu
	int	m_iWidth, m_iHeight; //largeur et hauteur �cran aire de jeu
	int	m_iFrameDelay; 	//laps de temps entre deux cycles de jeu
	BOOL	m_bSleep;	//sp�cifie si le jeu est en pause
	vector<Sprite*> m_vSprites;

	World* m_world;
	
	// Helper Methods
	BOOL				CheckSpriteCollision(Sprite* pTestSprite);

//Constructeur Destructeur
  public:
  GameEngine(HINSTANCE hInstance, LPTSTR szWindowClass, LPTSTR szTitle,
            	WORD wIcon, WORD wSmallIcon, int iWidth = 640, int iHeight = 480);
  virtual ~GameEngine();  //on ajoutera du code pour nettoyage

//M�thodes g�n�rales
	//Acc�der au pointeur du moteur
  static GameEngine*  GetEngine()  { return m_pGameEngine; };
	//Initialisera le programme du jeu quand moteur cr�er
  BOOL      Initialize(int iCmdShow);
	//G�rer les �v�nements Windows au sein du jeu
  LRESULT   HandleEvent(HWND hWindow, UINT msg, 
    WPARAM wParam, LPARAM lParam);

  void                ErrorQuit(LPTSTR szErrorMsg);
  void                AddSprite(Sprite* pSprite);
  void				SetWorld(World* pWorld){m_world = pWorld;};
  World*			GetWorld(){return m_world;};
  void                DrawSprites(HDC hDC);
  void				DrawWorld(HDC hDC){if (m_world != NULL) m_world->Draw(hDC);};
  void                UpdateSprites();
  void                CleanupSprites();
  void				RemoveWorld(){m_world = 0;};
  Sprite*           IsPointInSprite(int x, int y);

//M�thodes accesseurs  (acc�der ou ajuster des variables membres)
  HINSTANCE GetInstance() { return m_hInstance; };
  HWND   GetWindow() 	{ return m_hWindow; };
  void		SetWindow(HWND hWindow)	{ m_hWindow = hWindow; };
  LPTSTR	GetTitle() 	{ return m_szTitle; };
  WORD	GetIcon() 	{ return m_wIcon; };
  WORD	GetSmallIcon() {return m_wSmallIcon;};
  int			GetWidth() 	{ return m_iWidth; };
  int			GetHeight() 	{ return m_iHeight; };
  int			GetFrameDelay() 	{ return m_iFrameDelay; };
	//Ajuste fr�quence de d�filement des images(millisec)
  void		SetFrameRate(int iFrameRate) 
	             	{ m_iFrameDelay = 1000 /iFrameRate; };
  int			GetFrameRate() {return 1000 / m_iFrameDelay;};
  BOOL		GetSleep() 	{ return m_bSleep; };
  void		SetSleep(BOOL bSleep) 	{ m_bSleep = bSleep; };
};
