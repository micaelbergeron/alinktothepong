#include "Button.h"

Button::Button(Bitmap* _sprite, Bitmap* _clickedSprite, int x, int y, void (*_onClick)(Button* sender))
{
	m_sprite = _sprite;
	m_clickedSprite = _clickedSprite;
	posX = x;
	posY = y;
	OnClick = _onClick;
	m_parent = NULL;
	m_pressed = false;
}

bool Button::CheckClick(int x, int y)
{
	// Rectangle collision test.
	return (( x >= this->posX && x <= (this->posX + m_sprite->GetWidth())) &&
		( y >= this->posY && y <= (this->posY + m_sprite->GetHeight())));
}

void Button::Draw(HDC hDC)
{
	// TODO : Test de clique
	if (m_pressed)
		m_clickedSprite->Draw( hDC, posX, posY, true);
	else
		m_sprite->Draw(hDC, posX, posY, true);
}

void Button::Click()
{	
	if ((this->OnClick > 0) && m_pressed)
		OnClick(this);
	m_pressed = false;
}

Button::~Button(void)
{
	//delete m_sprite, m_clickedSprite;
}
