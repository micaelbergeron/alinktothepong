#include "Ball.h"

Ball::Ball(Bitmap* _idle, POINT _offSet) : Actor( -1, _idle, _offSet)
{
	SetAngle(0);
	SetState(BOUNCE);
	ShouldStop(false);

	m_lastHitter = NULL;
	m_invulCount = 0;
}

Ball::~Ball(void)
{
}

// La balle bouge selon un arc en degr�.
void Ball::Move(int speed)
{
	SetSpeed(speed);

	double resultX = cos( m_direction * PI/180);
	double resultY = sin( m_direction * PI/180);

	if ( ((m_direction >= 45) && (m_direction <=135) ||
		(m_direction >= 225) && (m_direction <=315)))
		m_ptVelocity.x = resultX * GetSpeed();
	else
		m_ptVelocity.x = -(resultX * GetSpeed());
	m_ptVelocity.y = resultY * GetSpeed();
}

void Ball::Draw(HDC hDC)
{
		Sprite::Draw(hDC, 0);
}

SPRITEACTION Ball::Update()
{
	m_invulCount --;
	if(m_invulCount == 0)
		SetState(BOUNCE);

	return Actor::Update();
}

// M�me logique que Sprite::Update
void Ball::Bounce(WorldObject* _collided)
{	
	// Ne pas rester coinc�.
	m_invulCount = 2;	

	RECT rcCollided = _collided->GetCollision();

	BOOL bBounce = FALSE;
	POINT ptNewPosition, ptSpriteSize, ptBoundsSize;
	ptNewPosition.x = m_rcPosition.left;
	ptNewPosition.y = m_rcPosition.top;
	ptSpriteSize.x = m_rcPosition.right - m_rcPosition.left;
	ptSpriteSize.y = m_rcPosition.bottom - m_rcPosition.top;

	POINT ptNewVelocity = m_ptVelocity;
	if (ptNewPosition.x < rcCollided.left)
		{
		  bBounce = TRUE;
		  ptNewPosition.x = rcCollided.left;
		  ptNewVelocity.x = -ptNewVelocity.x;
		}
	else if ((ptNewPosition.x + ptSpriteSize.x) > rcCollided.right)
		{
		  bBounce = TRUE;
		  ptNewPosition.x = rcCollided.right - ptSpriteSize.x;
		  ptNewVelocity.x = -ptNewVelocity.x;
		}
	if (ptNewPosition.y < rcCollided.top)
		{
		  bBounce = TRUE;
		  ptNewPosition.y = rcCollided.top;
		  ptNewVelocity.y = -ptNewVelocity.y;
		}
	else if ((ptNewPosition.y + ptSpriteSize.y) > rcCollided.bottom)
		{
		  bBounce = TRUE;
		  ptNewPosition.y = rcCollided.bottom - ptSpriteSize.y;
		  ptNewVelocity.y = -ptNewVelocity.y;
		}
	if (bBounce)
	{
	  SetVelocity(ptNewVelocity);
	  SetPosition(ptNewPosition);
	}
}

void Ball::Reset()
{
	m_invulCount = 0;
	SetState(BOUNCE);
	srand(GetTickCount());
	SetAngle(rand() % 360);
	Move(5);
}