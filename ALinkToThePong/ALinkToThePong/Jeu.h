// GLOBAL VARS
#pragma once

#include "resource.h"
#include "GameEngine.h"
#include "Bitmap.h"
#include <windows.h>
#include "Actor.h"
#include "Link.h"
#include "Button.h"
#include "Menu.h"
#include "Helper.h"
#include "MessageFactory.h"
#include "Ball.h"
#include "World.h"
#include "bass.h"
#include <stdio.h>

// Pause
void Pause(int seconds, void (*proc)() = NULL);
int _PAUSE_TICKS;
void (*m_afterPause)();
bool IsPaused(); 

// MENU_MAIN Events 
void btnJouer_Click(Button* sender);
void btnInst_Click(Button* sender);
void btnQuit_Click(Button* sender);
// MENU_INST Events
void btnOK_Click(Button* sender);
// MENU_LEVEL Events
void btnDesert_Click(Button* sender);
void btnForest_Click(Button* sender);
void btnBack_Click(Button* sender);
// MENU_SCORE Events
void btnContinue_Click(Button * sender);

void BackToMenu();
void SetStartPos();
void ShowWinner(Link* winner);
void StartBGM();
void Play();

void OnAttackCompleted(Actor* _sender);

// VARS

#define MAX_GOALS 3
#define DEF_BALL_SPEED 5
#define DEF_BLUE_BALL_SPEED 10

#define KEY_W 0x57
#define KEY_A 0x41
#define KEY_S 0x53
#define KEY_D 0x44
#define KEY_C 0x43

// Double buffering
HDC _hOffscreenDC;
HBITMAP _hOffscreenBitmap;

// Variable d'ex�cution
HMUSIC _JINGLE, _BGM;
EngineState CUR_ENGINE_STATE;
Menu * CUR_MENU;
Bitmap *CUR_BG, *CUR_LOGO, *CUR_HUD ;
Link * _PLAYER_1, *_PLAYER_2;
Ball * _BALL;

// Global vars
Bitmap  *_BG_MAIN, *_LOGO_MAIN, *_BG_INST, *_HUD;
HMUSIC BGM_OVERWORLD, BGM_TITLE, BGM_TITLE2, BGM_BOSS;
World* _DESERT_WORLD, *_FOREST_WORLD;

bool _JUST_SCORED, _SHOW_SCORE;
Menu *_MENU_MAIN, *_MENU_INST, * _MENU_SCORE, * _MENU_LEVEL;
Button* btnJouer;

HFONT gameFont = CreateFont(28, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, ANTIALIASED_QUALITY, 0, "Poor Richard");

// FPS
// Variables used to calculate frames per second:
/*
DWORD dwFrames;
DWORD dwCurrentTime;
DWORD dwLastUpdateTime;
DWORD dwElapsedTime;
TCHAR szFPS[32];
TCHAR szSwing[32];
TCHAR szState[32];
*/
TCHAR szPlayer1Buts[32];
TCHAR szPlayer2Buts[32];
TCHAR szWinner[100];

void StartBGM();
void DoGoal();