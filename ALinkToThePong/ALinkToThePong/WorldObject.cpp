#include "WorldObject.h"

WorldObject::WorldObject(int x, int y, int width, int height)
{
	m_rcPosition.left = x;
	m_rcPosition.top = y;
	m_rcPosition.right = x + width;
	m_rcPosition.bottom = y + height;
	m_bWalkthrough = false;
	m_bBallPassThrough = false;
	m_bNeedBlueToPass = false;
	m_isStatic = true;
	m_bGoal = false;
	CalcCollisionRect();
}

WorldObject::~WorldObject(void)
{
}

BOOL WorldObject::TestCollision(ICollision *_test)
{
	RECT& rcTest = _test->GetCollision();
	return m_rcCollision.left <= rcTest.right &&
         rcTest.left <= m_rcCollision.right &&
         m_rcCollision.top <= rcTest.bottom &&
         rcTest.top <= m_rcCollision.bottom;
}

