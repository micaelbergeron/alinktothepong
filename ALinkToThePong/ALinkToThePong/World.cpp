#include "World.h"

World::World(Bitmap* bmp) : Sprite(bmp)
{
	SetPosition(0,0);
	SetNumFrames(1);
	SetNumRowFrames(1);
	SetZOrder(-1);
	ICollision::m_isStatic = true;
}

World* World::CreateFromFile(HDC hDC ,char* path)
{
	string str(path);
	str.replace(str.end() - 3, str.end(), "bmp"); 
	const char* buffer = str.c_str();

	LPTSTR lptstr = LPTSTR(buffer);

	World * newWorld = new World(new Bitmap(hDC, lptstr));

	TiXmlDocument doc (path);
	doc.LoadFile();

	TiXmlNode* root = doc.FirstChild()->NextSibling();

	// Ajout des "Walls"
	TinyXPath::xpath_processor* proc = new TinyXPath::xpath_processor(root, "//objectgroup[@name='Walls']/object");
	TinyXPath::expression_result er = proc->er_compute_xpath();
	TinyXPath::node_set* ns_Walls = er.nsp_get_node_set();

	for (int i = 0; i < ns_Walls->u_get_nb_node_in_set(); i++)
	{
		const TiXmlNode* curNode = ns_Walls->XNp_get_node_in_set(i);
		const TiXmlElement* curElement = curNode->ToElement();

		int x, y ,width, height;
		curElement->Attribute("x", &x);
		curElement->Attribute("y", &y);
		curElement->Attribute("width", &width);
		curElement->Attribute("height", &height);

		WorldObject* obj = new WorldObject(x, y , width, height);
		newWorld->AddWorldObject(obj);
	}

	// Ajout des Obstacles
	proc = new TinyXPath::xpath_processor(root, "//objectgroup[@name='Obstacles']/object");
	er = proc->er_compute_xpath();
	TinyXPath::node_set* ns_Obstacles = er.nsp_get_node_set();
	for (int i = 0;i < ns_Obstacles->u_get_nb_node_in_set(); i++)
	{
		const TiXmlNode* curNode = ns_Obstacles->XNp_get_node_in_set(i);
		const TiXmlElement* curElement = curNode->ToElement();

		int x, y ,width, height;
		curElement->Attribute("x", &x);
		curElement->Attribute("y", &y);
		curElement->Attribute("width", &width);
		curElement->Attribute("height", &height);

		WorldObject* obj = new WorldObject(x, y , width, height);
		obj->SetBallPassThrough(true);

		bool needsBlueToPass = TinyXPath::o_xpath_bool(curNode, "//property[@name='bluetopass']/@value=1");
		if(needsBlueToPass)
		{
			obj->SetNeedBlueToPass(true);
			obj->SetBallPassThrough(false);
		}
		newWorld->AddWorldObject(obj);
	}

	// Ajout des Goals
	TinyXPath::xpath_processor* proc_goal = new TinyXPath::xpath_processor(root, "//objectgroup[@name='Goals']/object");
	TinyXPath::expression_result er_goal = proc_goal->er_compute_xpath();
	TinyXPath::node_set* ns_goal = er_goal.nsp_get_node_set();

	for (int i = 0; i < ns_goal->u_get_nb_node_in_set(); i++)
	{		
		const TiXmlNode* curNode = ns_goal->XNp_get_node_in_set(i);
		const TiXmlElement* curElement = curNode->ToElement();

		int x, y ,width, height;
		curElement->Attribute("x", &x);
		curElement->Attribute("y", &y);
		curElement->Attribute("width", &width);
		curElement->Attribute("height", &height);

		WorldObject* obj = new WorldObject(x, y , width, height);
		obj->SetIsGoal(true);

		newWorld->AddWorldObject(obj);
	}

	// Ajout des Startpos
	TinyXPath::xpath_processor* proc_start = new TinyXPath::xpath_processor(root, "//objectgroup[@name='Startpos']/object");
	TinyXPath::expression_result er_start = proc_start->er_compute_xpath();
	TinyXPath::node_set* ns_start = er_start.nsp_get_node_set();

	for (int i=0; i < ns_start->u_get_nb_node_in_set(); i++)
	{	
		POINT pt;
		int x,y, index;
		const TiXmlElement* elem = ns_start->XNp_get_node_in_set(i)->ToElement();

		elem->Attribute("x", &x);
		elem->Attribute("y", &y);

		index = TinyXPath::i_xpath_int(elem, "//properties/property[@name='index']/@value");
		pt.x = x;
		pt.y = y;

		newWorld->AddStartPos(pt, index);
	}
	return newWorld;
}

World::~World(void)
{
	int max = m_objects.size();
	for (int i = 0; i < max; i++)
		delete m_objects[i];
	m_objects.clear();
}

void World::Draw(HDC hDC)
{		
	Sprite::Draw(hDC, 0);
}

BOOL World::TestCollision(ICollision* pTest)
{
	vector<WorldObject* >::iterator it;
	for(it = m_objects.begin(); it != m_objects.end(); it++)
	{
		if ( (*it)->TestCollision(pTest))
		{
			m_collided = (*it);
			return true;
		}
	}
	return false;
}

void World::AddStartPos(POINT _startPos, int index)
{
	m_startPos[index] = _startPos;
}