#pragma once
#include "Sprite.h"

/*
Une tile repr�sente un "carr�" dans le jeu.
Pour creer une Tile, il faut utiliser la fonction statique CreateTile(...);
*/
class Tile : public Sprite
{
	bool m_block;
	Tile(Bitmap *_bmp);

	virtual void OnAnimEnd(){};
	virtual void OnAnimTick(){};
public:	
	  virtual int RefCode(){return 4;};
	~Tile(void);
	static Tile* CreateTile(Bitmap *_bmp, bool _doBlock);	
	void Draw(HDC hDC);
};
