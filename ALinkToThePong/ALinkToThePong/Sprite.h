//-----------------------------------------------------------------
// Sprite Object
// C++ Header - Sprite.h
//-----------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include <windows.h>
#include "Bitmap.h"
#include "ICollision.h"

//-----------------------------------------------------------------
// Custom Data Types
//-----------------------------------------------------------------
typedef WORD        SPRITEACTION;
const SPRITEACTION  
					SA_NONE   = 0x0000L,
                    SA_KILL   = 0x0001L;

typedef WORD        BOUNDSACTION;
const BOUNDSACTION  BA_STOP   = 0,
                    BA_WRAP   = 1,
                    BA_BOUNCE = 2,
                    BA_DIE    = 3;

//-----------------------------------------------------------------
// Sprite Class
//-----------------------------------------------------------------
class Sprite : public ICollision
{
protected:
  // Member Variables
  Bitmap*       m_pBitmap;
  POINT         m_ptVelocity;
  POINT		 m_OffSet; // Le d�calage d'un animation.
  int           m_iZOrder;
  RECT          m_rcBounds;
  BOUNDSACTION  m_baBoundsAction;
  BOOL          m_bHidden;
  BOOL			m_bDying;
  int m_iNumFrames, m_iNumRowFrames, m_iCurFrame;
  int m_iFrameDelay, m_iFrameTrigger;

  // Helper Methods
  virtual void  CalcCollisionRect();

  // Animation events;
  virtual void OnAnimTick(){};
  virtual void OnAnimEnd(){};

public:
  // Constructor(s)/Destructor
  Sprite(Bitmap* pBitmap);
  Sprite(Bitmap* pBitmap, RECT& rcBounds,
		 BOUNDSACTION baBoundsAction = BA_STOP);
  Sprite(Bitmap* pBitmap, POINT ptPosition, POINT ptVelocity, int iZOrder,
		 RECT& rcBounds, BOUNDSACTION baBoundsAction = BA_STOP);
  virtual ~Sprite();

  // General Methods
  virtual int RefCode(){return 0;};
  virtual bool OnCollide(Sprite* _hitee){return false;};
  virtual BOOL TestCollision(ICollision* pTestSprite);
  virtual SPRITEACTION  Update();
  virtual void Draw(HDC hdc){Draw(hdc, 0);};
  void Draw(HDC hDC, int _curY);

  void					UpdateFrame();
  BOOL                IsPointInside(int x, int y);
  // Accessor Methods
  

  void    SetPosition(int x, int y);
  void    SetNumFrames(int iNumFrames);
  int		GetNumFrames() { return m_iNumFrames; };

  void SetNumRowFrames(int _value);

  int GetNumRowFrames(){return m_iNumRowFrames;};

  void    SetFrameDelay(int iFrameDelay) { m_iFrameDelay = iFrameDelay; };
  void    SetPosition(POINT ptPosition);
  void    SetPosition(RECT& rcPosition)
									{ CopyRect(&m_rcPosition, &rcPosition); };
  void    OffsetPosition(int x, int y);
  POINT   GetVelocity()             { return m_ptVelocity; };
  Bitmap* GetBitmap() {return m_pBitmap;};
  void SetBitmap(Bitmap* _bmp) {m_pBitmap = _bmp;};
  void ChangeAnim(Bitmap* _bmp, int _frameDelay, int _nbFrame, POINT _offSet);

  void    SetVelocity(int x, int y);
  void    SetVelocity(POINT ptVelocity);
  BOOL    GetZOrder()               { return m_iZOrder; };
  void    SetZOrder(int iZOrder)    { m_iZOrder = iZOrder; };
  void    SetBounds(RECT& rcBounds) { CopyRect(&m_rcBounds, &rcBounds); };
  void    SetBoundsAction(BOUNDSACTION ba) { m_baBoundsAction = ba; };
  BOOL    IsHidden()                { return m_bHidden; };
  void    SetHidden(BOOL bHidden)   { m_bHidden = bHidden; };
  int     GetWidth()                { return (m_pBitmap->GetWidth() / m_iNumFrames); };
  int     GetHeight()               { return m_pBitmap->GetHeight() / m_iNumRowFrames; };
  void	  Kill()					{m_bDying = TRUE;}
};

//-----------------------------------------------------------------
// Sprite Inline Helper Methods
//-----------------------------------------------------------------
inline void Sprite::CalcCollisionRect()
{
  int iXShrink = (m_rcPosition.left - m_rcPosition.right) / 12;
  int iYShrink = (m_rcPosition.top - m_rcPosition.bottom) / 12;
  CopyRect(&m_rcCollision, &m_rcPosition);
  InflateRect(&m_rcCollision, iXShrink, iYShrink);
}

//-----------------------------------------------------------------
// Sprite Inline General Methods
//-----------------------------------------------------------------

inline BOOL Sprite::IsPointInside(int x, int y)
{
  POINT ptPoint;
  ptPoint.x = x;
  ptPoint.y = y;
  return PtInRect(&m_rcPosition, ptPoint);
}

//-----------------------------------------------------------------
// Sprite Inline Accessor Methods
//-----------------------------------------------------------------
inline void Sprite::SetPosition(int x, int y)
{
  OffsetRect(&m_rcPosition, x - m_rcPosition.left, y - m_rcPosition.top);
  CalcCollisionRect();
}

inline void Sprite::SetPosition(POINT ptPosition)
{
  OffsetRect(&m_rcPosition, ptPosition.x - m_rcPosition.left,
    ptPosition.y - m_rcPosition.top);
  CalcCollisionRect();
}

inline void Sprite::OffsetPosition(int x, int y)
{
  OffsetRect(&m_rcPosition, x, y);
  CalcCollisionRect();
}

inline void Sprite::SetVelocity(int x, int y)
{
  m_ptVelocity.x = x;
  m_ptVelocity.y = y;
}

inline void Sprite::SetVelocity(POINT ptVelocity)
{
  m_ptVelocity.x = ptVelocity.x;
  m_ptVelocity.y = ptVelocity.y;
}
