#include "Enum.h"

struct Direction
{
	VerticalDirection VerticalDir;
	HorizontalDirection HorizontalDir;

	int ToIndex()
	{
		int index = -1;
		if (HorizontalDir == HOR_NONE)
		{
			switch(VerticalDir)
			{
			case UP:
				index = 0;
				break;
			case DOWN:
				index = 2;
				break;
			}
		}
		else
		{
			switch(HorizontalDir)
			{
			case LEFT:
				index = 3;
				break;
			case RIGHT:
				index = 1;
				break;
			}
		}

		return index;
	}
};

