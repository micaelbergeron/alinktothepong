#pragma once
#include "sprite.h"

class Message :
	public Sprite
{
public:
	Message(Bitmap* _bmp);
	~Message(void);

	virtual void OnAnimEnd();
	virtual void Draw(HDC hDC);
};
