#include "Actor.h"

Actor::Actor(int maxHp, Bitmap* _idle, POINT _offSet):Sprite(_idle)
{
	m_dir.HorizontalDir = HOR_NONE;
	m_dir.VerticalDir = VERT_NONE;
	m_state = IDLE;	
	m_lastState = IDLE;
	m_hp = m_maxHp = maxHp;
	m_bAnimating = false;
	m_msg = NULL;

	AddAnim(_idle, -1, 1, _offSet, IDLE);
}

Actor::~Actor(void)
{
}

void Actor::Move(int speed)
{
	if (!CanDoAction(MOVING))
		return;

	SetState(MOVING);

	m_ptVelocity.x = (int)m_dir.HorizontalDir * speed;
	m_ptVelocity.y = (int)m_dir.VerticalDir * speed;
}

void Actor::Stop()
{
	if (CanDoAction(MOVING))
	{
		SetState(IDLE);
		b_shouldStop = false;
	}		

	m_ptVelocity.x = 0;
	m_ptVelocity.y = 0;
}

void Actor::Draw(HDC hDC)
{	
	int index = m_dir.ToIndex();
	Sprite::Draw(hDC, index);
}

bool Actor::CanDoAction(ActorState toDo)
{
	return (toDo >= m_state);
}

void Actor::AddAnim(Bitmap* _bmp, int frameDelay, int _nbFrame, POINT _offSet, ActorState _trigger)
{
	// Il ne peut y avoir qu'un seul trigger pour 1 animation.
	for (int i = 0; i < m_triggers.size(); i++)
	{
		if (m_triggers[i] == _trigger)
			throw _trigger;
	}

	m_anims.push_back(_bmp);
	m_frameDelays.push_back(frameDelay);
	m_triggers.push_back(_trigger);
	m_frameCounts.push_back(_nbFrame);
	m_OffSets.push_back(_offSet);
}

SPRITEACTION Actor::Update()
{
	// Logique d'animation
	int cursor = 0;
	bool exitLoop = false;
	if (HasToChangeAnim())
	{
		do
		{
			if (m_triggers[cursor] == GetState())
			{
				ChangeAnim( m_anims[cursor], m_frameDelays[cursor], m_frameCounts[cursor], m_OffSets[cursor]);
				exitLoop = true;
			}
			++cursor;
		}while ((!exitLoop) && (cursor < m_triggers.size() ));
	}

	SetState(m_state);

	// Test de demande d'arr�t
	if( b_shouldStop)
		Stop();

	// Test de vitalit�
	if (m_hp == 0)
		Kill();


	return Sprite::Update();
}


int Actor::GetTriggerIndex()
{
		for (int i = 0; i < m_triggers.size(); i ++)
		{
			if (m_triggers[i] == GetState())
				return i;
		}
		return -1;
}

void Actor::GainHP(int _amount)
{
	m_hp += _amount;
	// Revive
}

void Actor::LoseHP(int _amount)
{
	if ((m_maxHp > 0) && (m_hp > 0) && !m_bHidden)
		m_hp -= _amount;
	if (m_hp < 0)
		m_hp = 0;
}