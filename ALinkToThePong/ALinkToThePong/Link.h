#pragma once
#include "actor.h"
#include "Bitmap.h"
#include "bass.h"
#include "resource.h"

class Link :
	public Actor
{
	HMUSIC sfx;
	int m_dmg;			// Dommage caus�s par une attaque
	int m_swingArc;	// Angle de contact de l'�p�e. Ex : 90 (En avant);
	int m_exhaust;	// D�lai entre 2 attaque;
	int m_nbGoals;	// Nombre de buts;

	virtual void OnAnimTick();
	virtual void OnAnimEnd();
public:
	Link(int _dmg, int _maxHp, Bitmap* _idle, POINT _offSet);
	~Link(void);

	virtual int RefCode(){return 2;};
	virtual void Reset();

	void Attack();
	void DoDamage(Actor* _target);
	int AddGoal(){return ++m_nbGoals;};
	int GetGoals(){return m_nbGoals;};
	int ContactAngle();

	SPRITEACTION Update();
	int GetSwing() {return m_swingArc;};	
};