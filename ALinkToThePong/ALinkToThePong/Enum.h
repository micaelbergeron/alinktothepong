#pragma once

enum ActorState
{
	IDLE,
	MOVING,
	ATTACKING,
	BLOCKING,
	BOUNCE,
	DESTROY
};

enum VerticalDirection
{	
	UP = -1,
	VERT_NONE = 0,
	DOWN = 1
};

enum HorizontalDirection
{	
	LEFT = -1,
	HOR_NONE = 0,
	RIGHT = 1
};

enum EngineState
{
	MENU,
	GAME,
	PAUSE
};