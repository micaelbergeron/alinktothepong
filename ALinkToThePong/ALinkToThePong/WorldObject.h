#pragma once
#include "ICollision.h"

class WorldObject :
	public ICollision
{
	bool m_bWalkthrough;
	bool m_bBallPassThrough;
	bool m_bNeedBlueToPass;
	bool m_bGoal;
public:
	WorldObject(int x, int y, int width, int height);
	~WorldObject(void);

	void SetWalkthrough(bool _value){m_bWalkthrough = _value;};
	bool IsWalkable(){return m_bWalkthrough;};

	void SetBallPassThrough(bool _value){m_bBallPassThrough = _value;};
	bool BallPassThrough(){return m_bBallPassThrough;};

	void SetNeedBlueToPass(bool _value){m_bNeedBlueToPass = _value;};
	bool NeedBlueToPass(){return m_bNeedBlueToPass;};

	void SetIsGoal(bool _value){m_bGoal = _value;};
	bool IsGoal(){return m_bGoal;};

	virtual void CalcCollisionRect(){CopyRect(&m_rcCollision, &m_rcPosition);};
	virtual BOOL TestCollision(ICollision *_test);
};
