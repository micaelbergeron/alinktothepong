#pragma once
#include "Button.h"
#include <vector>

class Button;

class Menu
{	
	bool m_hidden;
	std::vector<Button*> m_items;
public:
	void ManageClick(int x, int y);
	void ManagePress(int x, int y);
	void AddButton(Button* newButton);
	void Draw(HDC hDC);

	void Hide(){m_hidden = true;};
	void Show(){m_hidden = false;};

	Menu(void);
	~Menu(void);
};
