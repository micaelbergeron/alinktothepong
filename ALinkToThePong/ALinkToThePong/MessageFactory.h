#pragma once
#include "Message.h"
#include "resource.h"

class MessageFactory
{
	static HDC msgDC;
public:
	static void SetHDC(HDC hDC);
	static Message* createMessage(int msgID, HINSTANCE _hInst);
};
