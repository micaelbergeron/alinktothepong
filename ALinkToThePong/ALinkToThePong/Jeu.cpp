#include "Jeu.h"

int GameInitialize(HINSTANCE hInstance)
{
	//// Load bmp
	//bg = Bitmap(hDC, L"Resource\\bg\\bg.bmp");
	// Zero out the frames per second variables:

	/*
	dwFrames = 0;
	dwCurrentTime = 0;
	dwLastUpdateTime = 0;
	dwElapsedTime = 0;
	szFPS[0] = '\0';
	*/

	_SHOW_SCORE = false;
	_PAUSE_TICKS = 0;
	_JUST_SCORED = false;

	// Create the GameEngine
	GameEngine* eng = new GameEngine(hInstance, "Game Window", "A Link to the Pong", IDB_BIG_ICON, IDB_ICON, 800, 600);
	BASS_Init(-1, 44100, 0, GameEngine::GetEngine()->GetWindow(), 0);

	BGM_BOSS = BASS_StreamCreateFile(false, MP3_BOSS, 0, 0, BASS_SAMPLE_LOOP);
	BGM_TITLE = BASS_StreamCreateFile(false, MP3_TITLE, 0, 0, BASS_SAMPLE_LOOP);
	BGM_OVERWORLD = BASS_StreamCreateFile(false, MP3_OVERWORLD, 0, 0, BASS_SAMPLE_LOOP);
	BGM_TITLE2 = BASS_StreamCreateFile(false, MP3_TITLE2, 0, 0, BASS_SAMPLE_LOOP);

	_BGM = BGM_TITLE;
	_JINGLE = BASS_StreamCreateFile(false, MP3_TRIFORCE, 0, 0, 0);

	return true;
}

void GameEnd()
{
	// Nettoyage du contexte de p�riph�rique et du bitmap hors �cran  
	DeleteObject(_hOffscreenBitmap);
	DeleteObject(gameFont);
	DeleteDC(_hOffscreenDC);

	delete _MENU_INST, _MENU_MAIN, _MENU_SCORE, _MENU_LEVEL;
	delete _BG_INST, _BG_MAIN ;
	delete _LOGO_MAIN;
	delete _HUD;

	delete _DESERT_WORLD, _FOREST_WORLD;

	BASS_Free();
	GameEngine::GetEngine()->CleanupSprites();
	return;
}

void GamePaint(HDC hDC)
{
	SetTextColor(hDC, RGB(255, 255, 255));
	SetBkMode(hDC, TRANSPARENT);
	wsprintf(szPlayer1Buts, "%u", _PLAYER_1->GetGoals());
	wsprintf(szPlayer2Buts, "%u", _PLAYER_2->GetGoals());

	// Le paint s'occupe d'un background et d'un menu, si il y en a un.
	
	if (CUR_BG != NULL)
		CUR_BG->Draw(hDC, 0, 0);	
	GameEngine::GetEngine()->DrawWorld(hDC);
	GameEngine::GetEngine()->DrawSprites(hDC);

	if (_SHOW_SCORE)
	{
		RECT rt;
		GetClientRect(GameEngine::GetEngine()->GetWindow(), &rt);
		FillRect(hDC, &rt, CreateSolidBrush(RGB(0,0,0)));
		TextOut(hDC, 245, 230, szWinner, lstrlen(szWinner));
	}

	if (CUR_MENU != NULL)
		CUR_MENU->Draw(hDC);
	if (CUR_LOGO != NULL)
		CUR_LOGO->Draw(hDC, 250, 60, true);

	// Draw FPS
	// Calculate the number of frames per one second:
	/*
	dwFrames++;
	dwCurrentTime = GetTickCount(); // Even better to use timeGetTime()
	dwElapsedTime = dwCurrentTime - dwLastUpdateTime;

	if(dwElapsedTime >= 1000)
	{
		wsprintf(szFPS, "FPS = %u", (UINT)(dwFrames * 1000.0 / dwElapsedTime));
		dwFrames = 0;
		dwLastUpdateTime = dwCurrentTime;
	}
	

	wsprintf(szSwing, "Swing = %u", (UINT)_PLAYER_1->GetSwing());
	wsprintf(szState, "State = %u", _PLAYER_1->GetState());	
	*/

	if (CUR_HUD != NULL)
	{
		CUR_HUD->Draw(hDC, 0, 540, true);
		if (!_SHOW_SCORE)
		{
			TextOut(hDC, 425, 558, szPlayer1Buts, lstrlen(szPlayer1Buts));
			TextOut(hDC, 640, 558, szPlayer2Buts, lstrlen(szPlayer2Buts));
		}
	}



	// Write the FPS onto the Memory DC.
	/*
	TextOut(hDC, 0, 0, szFPS, lstrlen(szFPS));
	TextOut(hDC, 100, 0, szSwing, lstrlen(szSwing));
	TextOut(hDC, 200, 0, szState, lstrlen(szState));
	*/
}

void GameDeactivate(HWND hWindow)
{
}

void GameActivate(HWND hWindow)
{
}

void GameStart(HWND hWindow)
{
  _hOffscreenDC = CreateCompatibleDC(GetDC(hWindow)); 
  _hOffscreenBitmap = CreateCompatibleBitmap(GetDC(hWindow), 
	  GameEngine::GetEngine()->GetWidth(), GameEngine::GetEngine()->GetHeight()); 
  SelectObject(_hOffscreenDC, _hOffscreenBitmap); 
  SelectObject(_hOffscreenDC, gameFont);

	CUR_ENGINE_STATE = MENU;
	HDC hdc = GetDC(hWindow);
	HINSTANCE hInst =  GameEngine::GetEngine()->GetInstance();

	_HUD = new Bitmap(hdc, IDB_HUD, hInst);

	_MENU_MAIN = new Menu();
	_MENU_MAIN->AddButton(new Button(new Bitmap(hdc, IDB_BTNJOUER, hInst), new Bitmap(hdc, IDB_BTNJOUER_P, hInst), 300, 300, &btnJouer_Click));
	_MENU_MAIN->AddButton(new Button(new Bitmap(hdc, IDB_BTNINST, hInst), new Bitmap(hdc, IDB_BTNINST_P, hInst), 300, 370, &btnInst_Click));
	_MENU_MAIN->AddButton(new Button(new Bitmap(hdc, IDB_BTNQUIT, hInst), new Bitmap(hdc, IDB_BTNQUIT_P, hInst), 300, 440, &btnQuit_Click));

	_MENU_INST = new Menu();
	_MENU_INST->AddButton( new Button(new Bitmap(hdc, IDB_BTNOK, hInst), new Bitmap(hdc, IDB_BTNOK_P, hInst), 20, 535, &btnOK_Click));
	
	_MENU_LEVEL = new Menu();
	Bitmap* desertBtn = new Bitmap(hdc, IDB_BTN_DESERT, hInst);
	Bitmap* forestBtn = new Bitmap(hdc, IDB_BTN_FOREST, hInst);
	_MENU_LEVEL->AddButton( new Button(desertBtn, desertBtn, 100, 100, &btnDesert_Click));
	_MENU_LEVEL->AddButton( new Button(forestBtn, forestBtn, 260, 100, &btnForest_Click));

	_MENU_SCORE = new Menu();
	_MENU_SCORE->AddButton(new Button(new Bitmap(hdc, IDB_BTNOK, hInst), new Bitmap(hdc, IDB_BTNOK_P, hInst), 300, 400, &btnContinue_Click));

	_PLAYER_1 = new Link(1, 10, new Bitmap(hdc, IDB_IDLE, hInst), CreatePoint(0, 0));
	_PLAYER_1->SetNumRowFrames(4);
	_PLAYER_1->AddAnim(new Bitmap(hdc, IDB_WALK, hInst), 1, 12, CreatePoint(0, 0), ActorState::MOVING);
	_PLAYER_1->AddAnim(new Bitmap(hdc, IDB_ATCK, hInst), 1, 6, CreatePoint(-5, 2), ActorState::ATTACKING);

	_PLAYER_2 = new Link(1, 10, new Bitmap(hdc, IDB_IDLE, hInst), CreatePoint(0, 0));
	_PLAYER_2->SetNumRowFrames(4);
	_PLAYER_2->AddAnim(new Bitmap(hdc, IDB_WALK, hInst), 1, 12, CreatePoint(0, 0), ActorState::MOVING);
	_PLAYER_2->AddAnim(new Bitmap(hdc, IDB_ATCK, hInst), 1, 6, CreatePoint(-5, 2), ActorState::ATTACKING);

	_BALL = new Ball(new Bitmap(hdc, IDB_BALL_DESTROY, hInst), CreatePoint(0,0));
	_BALL->SetNumRowFrames(1);
	_BALL->AddAnim(new Bitmap(hdc, IDB_BALL_BOUNCE, hInst), 1, 2, CreatePoint(0,0), BOUNCE);
	_BALL->AddAnim(new Bitmap(hdc, IDB_BALL_DESTROY, hInst), 1, 2, CreatePoint(0, 0), DESTROY);
	_BALL->SetBoundsAction(BA_BOUNCE);
	_BALL->SetPosition(400, 300);
	_BALL->Move(5);

	_DESERT_WORLD = World::CreateFromFile(hdc, "Resource\\maps\\desert.tmx");
	_FOREST_WORLD = World::CreateFromFile(hdc, "Resource\\maps\\forest.tmx");

	// Ajout des sprites a la liste des sprites.
	_PLAYER_1->SetHidden(true);
	_PLAYER_2->SetHidden(true);
	_BALL->SetHidden(true);

	GameEngine::GetEngine()->AddSprite(_PLAYER_1);
	GameEngine::GetEngine()->AddSprite(_PLAYER_2);
	GameEngine::GetEngine()->AddSprite(_BALL);

	_BG_MAIN = new Bitmap(hdc, IDB_BG, GameEngine::GetEngine()->GetInstance());
	_LOGO_MAIN = new Bitmap(hdc, IDB_LOGO, GameEngine::GetEngine()->GetInstance());
	_BG_INST = new Bitmap(hdc, IDB_BG_INST, hInst);

	CUR_BG = _BG_MAIN;
	CUR_LOGO = _LOGO_MAIN;
	CUR_MENU = _MENU_MAIN;

	StartBGM();
}

void GameCycle()
{
	if (IsPaused())
		return;
	if (_JUST_SCORED && !_SHOW_SCORE)
	{
		Pause(15, &StartBGM);
		_JUST_SCORED = false;
	}

	// Obtenir un contexte de p�riph�rique pour redessiner le jeu 
	HWND  hWindow = GameEngine::GetEngine()->GetWindow(); 
	HDC   hDC = GetDC(hWindow); 
	// pour dessiner le jeu hors �cran on passe le p�riph�rique hors �cran 
	// � la proc�dure GamePaint alors tout s'affiche en une fois 
	GamePaint(_hOffscreenDC); 
	// dessiner le bitmap hors �cran sur l'�cran de jeu 
	BitBlt(hDC, 0, 0, GameEngine::GetEngine()->GetWidth(), GameEngine::GetEngine()->GetHeight(), 
	   _hOffscreenDC, 0, 0, SRCCOPY); 
	
	GameEngine::GetEngine()->UpdateSprites();

	// Nettoyage 
	ReleaseDC(hWindow, hDC);	
}

void HandleKey()
{
	if (IsPaused())
		return;

	Direction moveDirP1, moveDirP2;
	moveDirP1.HorizontalDir = HOR_NONE;
	moveDirP1.VerticalDir = VERT_NONE;

	moveDirP2.HorizontalDir = HOR_NONE;
	moveDirP2.VerticalDir = VERT_NONE;

	// test de changement d'a4nim
	if (GetAsyncKeyState(KEY_C) != 0)
	{
		_PLAYER_1->Attack();
	}
	if (GetAsyncKeyState(VK_NUMPAD0) != 0)
	{
		_PLAYER_2->Attack();
	}

	if (GetAsyncKeyState(VK_UP) != 0)
		moveDirP2.VerticalDir = UP;
	if (GetAsyncKeyState(KEY_W) != 0)
		moveDirP1.VerticalDir = UP;

	if (GetAsyncKeyState(VK_RIGHT) != 0)
		moveDirP2.HorizontalDir = RIGHT;
	if (GetAsyncKeyState(KEY_D) != 0)
		moveDirP1.HorizontalDir = RIGHT;

	if (GetAsyncKeyState(VK_DOWN) != 0)
		moveDirP2.VerticalDir = DOWN;
	if (GetAsyncKeyState(KEY_S) != 0)
		moveDirP1.VerticalDir = DOWN;

	if (GetAsyncKeyState(VK_LEFT) != 0)
		moveDirP2.HorizontalDir = LEFT;
	if (GetAsyncKeyState(KEY_A) != 0)
		moveDirP1.HorizontalDir = LEFT;

	if ( (moveDirP1.HorizontalDir != HOR_NONE)  || (moveDirP1.VerticalDir != VERT_NONE))
	{
		_PLAYER_1->Move(6);
		_PLAYER_1->SetDirection(moveDirP1);
	}
	else
	{
		_PLAYER_1->ShouldStop();
	}

	if ( (moveDirP2.HorizontalDir != HOR_NONE) || (moveDirP2.VerticalDir != VERT_NONE))
	{
		_PLAYER_2->Move(6);
		_PLAYER_2->SetDirection(moveDirP2);
	}
	else
	{
		_PLAYER_2->ShouldStop();
	}
}

void MouseButtonDown(int x, int y, BOOL bLeft)
{
	if(CUR_MENU != NULL)
		CUR_MENU->ManagePress(x, y);
}

void MouseButtonUp(int x, int y, BOOL bLeft)
{
	if (CUR_MENU != NULL)
		CUR_MENU->ManageClick(x, y);
}

void MouseMove(int x, int y)
{
}

BOOL SpriteCollision(ICollision* pHitter, ICollision* pHittee)
{
	bool _hasEffect = false;

	// Link collisions
	Link* _linkHitter = dynamic_cast<Link*>(pHitter);

	if (_linkHitter != 0)
	{
		Ball* _ballHittee = dynamic_cast<Ball*>(pHittee);
		if (_ballHittee != 0) 
		// Link
		if (_linkHitter->GetState() == ATTACKING)
		{
			if (_ballHittee->IsHittable())
			{
				_hasEffect = true;
				_ballHittee->SetLastHitter(_linkHitter);
				int newAngle = _linkHitter->ContactAngle();
				int swing = _linkHitter->GetSwing();

				_ballHittee->SetAngle(newAngle);
				_ballHittee->SetInvulCount(20);
				// Perfect hit?
				if (swing == 45)
				{
					_ballHittee->Move(15);
					_ballHittee->SetInvulCount(50);
					_ballHittee->SetState(DESTROY);
				}
				else if ((swing >= 30) && (swing <= 60))
				{		
					_ballHittee->Move(10);
					_ballHittee->SetState(DESTROY);
					// Creation d'un nouveau message "OK"
					// _linkHitter->SetMessage(MessageFactory::createMessage(IDB_MSG_OK, GameEngine::GetEngine()->GetInstance()));
				}
				else
					_ballHittee->Move(5);
			}
		}

		World* _worldHittee = dynamic_cast<World*>(pHittee);
		if(_worldHittee != NULL)
		{
			WorldObject* col = _worldHittee->GetCollided();
			if (!col->IsWalkable())
				_hasEffect = true;
		}
	}

	// Ball collisions
	Ball* pBallHitter = dynamic_cast<Ball*>(pHitter);
	if (pBallHitter != NULL)
	{
		World* pWorldHitee = dynamic_cast<World*>(pHittee);
		if (pWorldHitee != NULL)
		{
			WorldObject* col = pWorldHitee->GetCollided();
			if (col->IsGoal())
			{
				if (pBallHitter->GetLastHitter()->AddGoal() == MAX_GOALS)
				{
					ShowWinner(pBallHitter->GetLastHitter());
				}
				else
				{
					// Reset la position des units et fait joueur un Jingle
					DoGoal();
				}
			}
			else if (col->NeedBlueToPass() && pBallHitter->GetState() == DESTROY)
			{
				_hasEffect = false;
				pBallHitter->SetInvulCount(5);
			}
			else if (!col->BallPassThrough())
			{
				pBallHitter->Bounce(col);
				_hasEffect = true;
			}

		}
	}

	return _hasEffect;
}

void Pause(int seconds, void (*proc)())
{
	_PAUSE_TICKS = seconds * (GameEngine::GetEngine()->GetFrameRate());
	if( proc != NULL)
		m_afterPause = proc;
}

bool IsPaused()
{
	if (--_PAUSE_TICKS < 0)
	{
		if (m_afterPause != NULL)
		{
			(*m_afterPause)();
			m_afterPause = 0;
		}			
		return false;
	}
	return true;
}

void BackToMenu()
{	
	GameEngine::GetEngine()->RemoveWorld();
	CUR_MENU = _MENU_MAIN;
	CUR_LOGO = _LOGO_MAIN;
	CUR_BG = _BG_MAIN;

	_PLAYER_1->SetHidden(true);
	_PLAYER_2->SetHidden(true);
	_BALL->SetHidden(true);

	CUR_ENGINE_STATE = MENU;
	StartBGM();
}

void DoGoal()
{
	_JUST_SCORED = true;
	BASS_ChannelStop(_BGM);
	BASS_ChannelPlay(_JINGLE, false);
	SetStartPos();
	_BALL->Reset();
}

void SetStartPos()
{
	World* wd = GameEngine::GetEngine()->GetWorld();
	_PLAYER_1->SetPosition( wd->GetStartPosition(0) );
	_PLAYER_2->SetPosition( wd->GetStartPosition(1) );
	_BALL->SetPosition(wd->GetStartPosition(2) );
}

void StartBGM()
{
	BASS_ChannelStop(_BGM);
	// Selection de BGM;
	if (CUR_ENGINE_STATE == EngineState::MENU)
		_BGM = BGM_TITLE;
	else if (_PLAYER_1->GetGoals() + _PLAYER_2->GetGoals() < 3)
		_BGM = BGM_OVERWORLD;
	else
		_BGM = BGM_BOSS;

	BASS_ChannelPlay(_BGM, true);
}

void Play()
{
	CUR_MENU = NULL;
	CUR_LOGO = NULL;
	CUR_BG = NULL;
	CUR_HUD = _HUD;
	CUR_ENGINE_STATE = GAME;

	StartBGM();
	SetStartPos();

	_BALL->Reset();
	_PLAYER_1->Reset();
	_PLAYER_2->Reset();

	_PLAYER_1->SetHidden(false);
	_PLAYER_2->SetHidden(false);
	_BALL->SetHidden(false);
}

void ShowWinner(Link* winner)
{
	_SHOW_SCORE = true;
	wsprintf(szWinner, "Le Joueur %u a gagn� la partie !", winner == _PLAYER_1 ? 1 : 2);
	CUR_MENU = _MENU_SCORE;
	CUR_HUD = NULL;
	CUR_ENGINE_STATE = MENU;

}
// Menu Events
void btnJouer_Click(Button* sender)
{
	CUR_ENGINE_STATE = MENU;
	CUR_LOGO = NULL;
	CUR_MENU = _MENU_LEVEL;
}

void btnInst_Click(Button* sender)
{
	CUR_ENGINE_STATE = MENU;
	CUR_BG = _BG_INST;
	CUR_LOGO = NULL;
	CUR_MENU = _MENU_INST;
}
void btnQuit_Click(Button* sender)
{
	PostQuitMessage(0);
}

void btnOK_Click(Button* sender)
{
	CUR_ENGINE_STATE = MENU;
	CUR_BG = _BG_MAIN;
	CUR_MENU = _MENU_MAIN;
	CUR_LOGO = _LOGO_MAIN;
}

// MENU_LEVEL Events
void btnDesert_Click(Button* sender)
{
	GameEngine::GetEngine()->SetWorld(_DESERT_WORLD);
	Play();
}

void btnForest_Click(Button* sender)
{
	GameEngine::GetEngine()->SetWorld(_FOREST_WORLD);
	Play();
}
void btnBack_Click(Button* sender)
{
	CUR_ENGINE_STATE = MENU;
	CUR_MENU = _MENU_MAIN;
}

// MENU_SCORE Events
void btnContinue_Click(Button * sender)
{
	BackToMenu();
	_SHOW_SCORE = false;
}