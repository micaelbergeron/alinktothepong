#pragma once
#include "Enum.h"
#include "Struct.h"
#include "Bitmap.h"
#include "Sprite.h"
#include "Message.h"
#include <vector>
#include <map>

// Classe de base de tout ce qui bouge
class Actor : public Sprite
{
private:
	std::vector<Bitmap*> m_anims;
	std::vector<int> m_frameDelays;
	std::vector<int> m_frameCounts;
	std::vector<ActorState> m_triggers; 
	std::vector< POINT > m_OffSets;

	int m_maxHp; // -1 invulnerable
	int m_hp;
	bool b_shouldStop;
		
	void Stop();
	int GetTriggerIndex();

	Message* m_msg;
protected:
	ActorState m_state, m_lastState;
	Direction m_dir;
	bool m_bAnimating;

public:	
	virtual int RefCode(){return 1;};
	virtual void Move(int speed);
	virtual SPRITEACTION Update();
	virtual void Draw(HDC hDC);
	virtual void Reset() = 0;


	Actor(int maxHp, Bitmap* _idle, POINT _offSet);
	~Actor(void);
	
	bool CanDoAction(ActorState toDo);

	ActorState GetState() {return m_state;};
	void SetState(ActorState _newState) { m_lastState = m_state; m_state = _newState;};
	bool HasToChangeAnim() {return m_lastState != m_state;};
	// L'animation sera ex�cute selon le predicate
	void AddAnim(Bitmap* _bmp, int frameDelay, int _nbFrame, POINT _offSet, ActorState _trigger);
	void SetDirection(Direction _dir){m_dir = _dir;};
	Direction GetDirection(){return m_dir;};
	void SetMessage(Message* _msg){m_msg = _msg;};
	void ShouldStop(bool _value = true) {b_shouldStop = _value;};
	void LoseHP(int _amount);
	void GainHP(int _amount);
};