#pragma once
#include "Bitmap.h"
#include "Menu.h"

class Menu;

class Button
{
	int posX, posY;
	Bitmap * m_sprite;
	Bitmap * m_clickedSprite;
	Menu* m_parent;
	bool m_pressed;

public:
	void (*OnClick)(Button*);
	Button(Bitmap* _bmp, Bitmap* _clickedBmp, int x, int y, void (*_onClick)(Button* sender));

	bool CheckClick(int x, int y);
	void Click();
	void Draw(HDC hDC);
	void SetParent(Menu* _menu){m_parent = _menu;};
	void SetPressed(bool _pressed){m_pressed = _pressed;};
	Menu* GetParent() {return m_parent;};

	~Button(void);
};